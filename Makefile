# Source files listed (.c)
OBJS = helloOpenGL.c

# Specify the compiler used
CC = gcc

# Specify where MinGW is installed
MINGW_INSTALLATION = C:/MinGW

# Specify additional include paths needed
INCLUDE_PATHS = -I$(MINGW_INSTALLATION)/include

# Specify where paths where additional .lib files can be found
LIBRARY_PATHS = -L$(MINGW_INSTALLATION)/lib

# Specify compile options
# -Wall gives a lot of warnings from shady code
# -Wl,-subsystem,windows gets rid of the console window (add if you want)
COMPILER_FLAGS = -Wall

# Specify linker flags
LINKER_FLAGS = -lmingw32 -lSDL2main -lSDL2 -lOpenGL32 -lglu32

# Specify name for our executable
BINARY_NAME = helloOpenGL

# Default make target that depends on source files
all : $(OBJS)
	mkdir -p build
	cp $(MINGW_INSTALLATION)/bin/SDL2.dll build
	$(CC) $(OBJS) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o build/$(BINARY_NAME)
