#include <SDL2/SDL.h>
#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>

// Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

// The window we'll be rendering to
SDL_Window* sdlWindow = NULL;

// Our opengl context handle
SDL_GLContext glContext = NULL;


int init()
{
	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 0;
	}

	// Create window
	sdlWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if (sdlWindow == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		return 0;
	}

	// Request these OpenGL context attributes
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	// Create our opengl context and attach it to our window
	glContext = SDL_GL_CreateContext(sdlWindow); 	

	// Print the OpenGL version we actually got
	int majorVersion = 0, minorVersion = 0;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &majorVersion);
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &minorVersion);
	printf("Got OpenGL version %i.%i\n", majorVersion, minorVersion);
	printf("Vendor: %s\n", glGetString(GL_VENDOR));
	printf("Renderer: %s\n", glGetString(GL_RENDERER));
	
	// This makes our buffer swap syncronized with the monitor's vertical refresh
	SDL_GL_SetSwapInterval(1);

	return 1;
}


void destroy()
{
	// Delete our OpengL context
	SDL_GL_DeleteContext(glContext);

	// Destroy window
	SDL_DestroyWindow(sdlWindow);

	// Shutdown SDL2
	SDL_Quit();
}


void render()
{
	// Set OpenGL clear color (green)
	glClearColor(0.0, 1.0, 0.0, 1.0);

	// Clear OpenGL color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	// Swap buffers so we can see what's rendered
	SDL_GL_SwapWindow(sdlWindow);
}


int main(int argc, char* args[])
{
	if (init() == 0) {
		return EXIT_FAILURE;
	}

	render();

	// Wait two seconds
	SDL_Delay(2000);

	destroy();

	return EXIT_SUCCESS;
}
